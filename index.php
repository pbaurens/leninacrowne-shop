<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--------------------------------FONTS----------------------------------------->
    <link href="https://fonts.googleapis.com/css2?family=Cousine:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <!-- CSS only -->
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/mediaqueries.css">
    <script src="https://kit.fontawesome.com/4af066ad9c.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <title>LeninaCrowneShop</title>
</head>
<body>
    <header  id="nav" class="header">
        <a href="#home" class="logo"><img src="assets/img/brand/LogoLenina-space-bordeau.gif"></a>
        <input class="menu-btn" type="checkbox" id="menu-btn" />
        <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
        <ul class="menu">
          <li><a href="#home">Accueil</a></li>
          <li><a href="#tshirts">T-Shirts</a></li>
          <li><a href="#cd">CD</a></li>
          <li><a href="#vinyl">Vinyl</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
    </header>
    <main>
        <div id="home" class="container site">
            <div class="banner-lenina">
                <img src="assets/img/banner/LeninaOfficialShop-white-1366x768.png">
            </div>
            <div id="tshirts"></div>
            <div class="wrapper-articles">
                <div class="wrapper-title">
                    <h2>T-SHIRTS</h2>
                    <div class="tshirts-articles">
                        <div class="bg-grey white-logo">
                            <a href="#"><img src="assets/img/tshirts/Tshirts-lenina-noir-classic-logo-blanc.png"></a>
                            <div class="text-articles">                          
                                <p>Logo classic Blanc</p>
                                <p>T-shirt</p>
                                <p>12€</p>
                            </div> 
                        </div>
                        <div class="bg-grey brown-logo">
                            <a href="#"><img src="assets/img/tshirts/Tshirts-lenina-noir-logo-marron-animals.png"></a>
                            <div class="text-articles">
                                <p>Logo marron</p>
                                <p>'Animals'</p>
                                <p>T-shirt</p>
                                <p>12€</p>
                            </div>
                        </div>
                        <div class="bg-grey duality-logo">
                            <a href="#"><img src="assets/img/tshirts/Tshirts-lenina-noir-classic-logo-duality.png"></a>
                            <div class="text-articles">
                                <p>Logo Duality</p>
                                <p>T-shirt</p>
                                <p>18€</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cd"></div>
                <div class="wrapper-title">
                    <h2>CD</h2>
                    <div class="cd-articles">
                        <div class="bg-grey last-ep">
                            <a href="#"><img src="assets/img/album/cover-last.jpg"></a>
                            <div class="text-articles">                          
                                <p>Last EP 2018</p>
                                <p>CD</p>
                                <p>10€</p>
                            </div> 
                        </div>
                        <div class="bg-grey duality-ep">
                            <a href="#"><img src="assets/img/album/cover-duality.jpg"></a>
                            <div class="text-articles">
                                <p>Duality EP 2016</p>
                                <p>CD</p>
                                <p>10€</p>
                            </div>
                        </div>
                        <div class="bg-grey lenina-ep">
                            <a href="#"><img src="assets/img/album/cover-lenina.jpg"></a>
                            <div class="text-articles">
                                <p>Lenina Crowne LP 2013</p>
                                <p>CD</p>
                                <p>10€</p>
                            </div>
                        </div>
                        <div class="bg-grey lipsick-lp">
                            <a href="#"><img src="assets/img/album/cover-lipsick.jpg"></a>
                            <div class="text-articles">
                                <p>Lipsick LP 2011</p>
                                <p>CD</p>
                                <p>8€</p>
                            </div>
                        </div>
                        <div class="bg-grey vivre-lp">
                            <a href="#"><img src="assets/img/album/cover-vivreetlaisserpourrir.jpg"></a>
                            <div class="text-articles">
                                <p>Vivre et laisser pourrir LP 2008</p>
                                <p>CD</p>
                                <p>8€</p>
                            </div>
                        </div>
                        <div class="bg-grey next">
                            <a href="#"><img src="assets/img/album/cover-nextAlbumAnimals.png"></a>
                            <div class="text-articles">
                                <p>Next</p>
                                <p>CD</p>
                                <p>0€</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="vinyl"></div>
                <div class="wrapper-title">
                    <h2>VINYL</h2>
                    <div class="cd-articles">
                        <div class="bg-grey last-ep">
                            <a href="#"><img src="assets/img/vinyl/Last-LP-vinyl.jpg"></a>
                            <div class="text-articles">                          
                                <p>Last EP 2018</p>
                                <p>Vinyl</p>
                                <p>14€</p>
                            </div> 
                        </div>
                        <div class="bg-grey duality-ep">
                            <a href="#"><img src="assets/img/vinyl/Duality-LP-vinyl.jpg"></a>
                            <div class="text-articles">
                                <p>Duality EP 2016</p>
                                <p>Vinyl</p>
                                <p>14€</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>

<!--------------------------------------------------------BUP----------------------------------------------->




   <?php
     /*require 'admin/database.php';
     echo '<header class="header">
            <a href="#home" class="logo"><img src="assets/img/brand/LogoLenina-space-bordeau.gif"></a>
            <input class="menu-btn" type="checkbox" id="menu-btn" />
            <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
            <ul class="menu">
                <li><a href="#0">Accueil</a></li>';
    $db = Database::connect();
    $statement = $db->query('SELECT * FROM $categories');
    $categories = $statement->fetchAll();
    foreach($categories as $category)
    {
        if($category['id'] == '1')
            echo '<li><a href="#' . $category['id'] . '">' . $category['name']. '</a></li>';

    }
    echo ' </ul>
    </header>';
    
    ?> */ 